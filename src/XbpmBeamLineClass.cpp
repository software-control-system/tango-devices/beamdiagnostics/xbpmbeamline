static const char *ClassId    = "$Id: XbpmBeamLineClass.cpp,v 1.7 2013-02-14 17:51:19 buteau Exp $";
static const char *CvsPath    = "$Source: /users/chaize/newsvn/cvsroot/BeamDiag/XbpmBeamLine/src/XbpmBeamLineClass.cpp,v $";
static const char *SvnPath    = "$HeadURL: $";
static const char *RcsId     = "$Header: /users/chaize/newsvn/cvsroot/BeamDiag/XbpmBeamLine/src/XbpmBeamLineClass.cpp,v 1.7 2013-02-14 17:51:19 buteau Exp $";
static const char *TagName   = "$Name: not supported by cvs2svn $";
static const char *HttpServer= "http://www.esrf.fr/computing/cs/tango/tango_doc/ds_doc/";
//+=============================================================================
//
// file :        XbpmBeamLineClass.cpp
//
// description : C++ source for the XbpmBeamLineClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the XbpmBeamLine once per process.
//
// project :     TANGO Device Server
//
// $Author: buteau $
//
// $Revision: 1.7 $
//
// $Log: not supported by cvs2svn $
// Revision 1.6  2013/02/14 17:51:05  buteau
// Dcoumentation modification to have a correct html doc
//
// Revision 1.5  2010/03/26 10:03:17  vince_soleil
// "Migration_Tango7_Part2"
//
// Revision 1.4  2009/03/03 14:11:07  anoureddine
// Mantis ID#11305:
// - Add StartAtInit properties in order to start acquisition at initialisation [default value =false].
// - Create (SAI, Locum) device proxy in init_device().
// - Commands and attributes are not allowed when state is FAULT.
// - Change Management of state & status.
//
// Revision 1.3  2008/03/18 09:19:02  sebleport
// enableAutoRange is now a memorized attribute
//
// Revision 1.2  2007/12/07 13:22:56  sebleport
// - alarm management modified
// - in geometry 2 the X and Z calculations have changed
//
// Revision 1.1  2007/12/06 08:14:02  stephle
// initial import
//
//
// copyleft :   European Synchrotron Radiation Facility
//              BP 220, Grenoble 38043
//              FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <XbpmBeamLine.h>
#include <XbpmBeamLineClass.h>


//+----------------------------------------------------------------------------
/**
 *	Create XbpmBeamLineClass singleton and return it in a C function for Python usage
 */
//+----------------------------------------------------------------------------
extern "C" {
#ifdef WIN32

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_XbpmBeamLine_class(const char *name) {
		return XbpmBeamLine_ns::XbpmBeamLineClass::init(name);
	}
}


namespace XbpmBeamLine_ns
{

//+----------------------------------------------------------------------------
//
// method : 		SetUnitCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *SetUnitCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "SetUnitCmd::execute(): arrived" << endl;

	Tango::DevUShort	argin;
	extract(in_any, argin);

	((static_cast<XbpmBeamLine *>(device))->set_unit(argin));
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		StartCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StartCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StartCmd::execute(): arrived" << endl;

	((static_cast<XbpmBeamLine *>(device))->start());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		StopCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be excuted
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *StopCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "StopCmd::execute(): arrived" << endl;

	((static_cast<XbpmBeamLine *>(device))->stop());
	return new CORBA::Any();
}


//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
XbpmBeamLineClass *XbpmBeamLineClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::XbpmBeamLineClass(string &s)
// 
// description : 	constructor for the XbpmBeamLineClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
XbpmBeamLineClass::XbpmBeamLineClass(string &s):DeviceClass(s)
{

	cout2 << "Entering XbpmBeamLineClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving XbpmBeamLineClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::~XbpmBeamLineClass()
// 
// description : 	destructor for the XbpmBeamLineClass
//
//-----------------------------------------------------------------------------
XbpmBeamLineClass::~XbpmBeamLineClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
XbpmBeamLineClass *XbpmBeamLineClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new XbpmBeamLineClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

XbpmBeamLineClass *XbpmBeamLineClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void XbpmBeamLineClass::command_factory()
{
	command_list.push_back(new SetUnitCmd("SetUnit",
		Tango::DEV_USHORT, Tango::DEV_VOID,
		"1: nA   or   2 : microA   or   3 : mA",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new StartCmd("Start",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));
	command_list.push_back(new StopCmd("Stop",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"nothing",
		"nothing",
		Tango::OPERATOR));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum XbpmBeamLineClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum XbpmBeamLineClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum XbpmBeamLineClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void XbpmBeamLineClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new XbpmBeamLine(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: XbpmBeamLineClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void XbpmBeamLineClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : quadrant1
	quadrant1Attrib	*quadrant1 = new quadrant1Attrib();
	Tango::UserDefaultAttrProp	quadrant1_prop;
	quadrant1_prop.set_label("I1");
	quadrant1_prop.set_unit(" ");
	quadrant1_prop.set_standard_unit(" ");
	quadrant1_prop.set_display_unit(" ");
	quadrant1_prop.set_format("%1.4e");
	quadrant1_prop.set_description("average current calculated over N samples on channel 1");
	quadrant1->set_default_properties(quadrant1_prop);
	att_list.push_back(quadrant1);

	//	Attribute : quadrant2
	quadrant2Attrib	*quadrant2 = new quadrant2Attrib();
	Tango::UserDefaultAttrProp	quadrant2_prop;
	quadrant2_prop.set_label("I2");
	quadrant2_prop.set_unit(" ");
	quadrant2_prop.set_standard_unit(" ");
	quadrant2_prop.set_display_unit(" ");
	quadrant2_prop.set_format("%1.4e");
	quadrant2_prop.set_description("average current calculated over N samples on channel 2");
	quadrant2->set_default_properties(quadrant2_prop);
	att_list.push_back(quadrant2);

	//	Attribute : quadrant3
	quadrant3Attrib	*quadrant3 = new quadrant3Attrib();
	Tango::UserDefaultAttrProp	quadrant3_prop;
	quadrant3_prop.set_label("I3");
	quadrant3_prop.set_unit(" ");
	quadrant3_prop.set_standard_unit(" ");
	quadrant3_prop.set_display_unit(" ");
	quadrant3_prop.set_format("%1.4e");
	quadrant3_prop.set_description("average current calculated over N samples on channel 3");
	quadrant3->set_default_properties(quadrant3_prop);
	att_list.push_back(quadrant3);

	//	Attribute : quadrant4
	quadrant4Attrib	*quadrant4 = new quadrant4Attrib();
	Tango::UserDefaultAttrProp	quadrant4_prop;
	quadrant4_prop.set_label("I4moy");
	quadrant4_prop.set_unit(" ");
	quadrant4_prop.set_standard_unit(" ");
	quadrant4_prop.set_display_unit(" ");
	quadrant4_prop.set_format("%1.4e");
	quadrant4_prop.set_description("average current calculated over N samples on channel 4");
	quadrant4->set_default_properties(quadrant4_prop);
	att_list.push_back(quadrant4);

	//	Attribute : intensity
	intensityAttrib	*intensity = new intensityAttrib();
	Tango::UserDefaultAttrProp	intensity_prop;
	intensity_prop.set_label("intensity");
	intensity_prop.set_unit(" ");
	intensity_prop.set_standard_unit(" ");
	intensity_prop.set_display_unit(" ");
	intensity_prop.set_format("%1.4e");
	intensity_prop.set_description("tsum of the 4 quadrants");
	intensity->set_default_properties(intensity_prop);
	att_list.push_back(intensity);

	//	Attribute : standardDeviationIntensity1
	standardDeviationIntensity1Attrib	*standard_deviation_intensity1 = new standardDeviationIntensity1Attrib();
	Tango::UserDefaultAttrProp	standard_deviation_intensity1_prop;
	standard_deviation_intensity1_prop.set_label("sigma I1");
	standard_deviation_intensity1_prop.set_unit(" ");
	standard_deviation_intensity1_prop.set_standard_unit(" ");
	standard_deviation_intensity1_prop.set_display_unit(" ");
	standard_deviation_intensity1_prop.set_format("%1.4e");
	standard_deviation_intensity1_prop.set_description("standard Deviation Intensity on channel 1");
	standard_deviation_intensity1->set_default_properties(standard_deviation_intensity1_prop);
	standard_deviation_intensity1->set_disp_level(Tango::EXPERT);
	att_list.push_back(standard_deviation_intensity1);

	//	Attribute : standardDeviationIntensity2
	standardDeviationIntensity2Attrib	*standard_deviation_intensity2 = new standardDeviationIntensity2Attrib();
	Tango::UserDefaultAttrProp	standard_deviation_intensity2_prop;
	standard_deviation_intensity2_prop.set_label("sigma I2");
	standard_deviation_intensity2_prop.set_unit(" ");
	standard_deviation_intensity2_prop.set_standard_unit(" ");
	standard_deviation_intensity2_prop.set_display_unit(" ");
	standard_deviation_intensity2_prop.set_format("%1.4e");
	standard_deviation_intensity2_prop.set_description("standard Deviation Intensity on channel 2");
	standard_deviation_intensity2->set_default_properties(standard_deviation_intensity2_prop);
	standard_deviation_intensity2->set_disp_level(Tango::EXPERT);
	att_list.push_back(standard_deviation_intensity2);

	//	Attribute : standardDeviationIntensity3
	standardDeviationIntensity3Attrib	*standard_deviation_intensity3 = new standardDeviationIntensity3Attrib();
	Tango::UserDefaultAttrProp	standard_deviation_intensity3_prop;
	standard_deviation_intensity3_prop.set_label("sigma I3");
	standard_deviation_intensity3_prop.set_unit(" ");
	standard_deviation_intensity3_prop.set_standard_unit(" ");
	standard_deviation_intensity3_prop.set_display_unit(" ");
	standard_deviation_intensity3_prop.set_format("%1.4e");
	standard_deviation_intensity3_prop.set_description("standard Deviation Intensity on channel 3");
	standard_deviation_intensity3->set_default_properties(standard_deviation_intensity3_prop);
	standard_deviation_intensity3->set_disp_level(Tango::EXPERT);
	att_list.push_back(standard_deviation_intensity3);

	//	Attribute : standardDeviationIntensity4
	standardDeviationIntensity4Attrib	*standard_deviation_intensity4 = new standardDeviationIntensity4Attrib();
	Tango::UserDefaultAttrProp	standard_deviation_intensity4_prop;
	standard_deviation_intensity4_prop.set_label("sigma I4");
	standard_deviation_intensity4_prop.set_unit(" ");
	standard_deviation_intensity4_prop.set_standard_unit(" ");
	standard_deviation_intensity4_prop.set_display_unit(" ");
	standard_deviation_intensity4_prop.set_format("%1.4e");
	standard_deviation_intensity4_prop.set_description("standard Deviation Intensity on channel 4");
	standard_deviation_intensity4->set_default_properties(standard_deviation_intensity4_prop);
	standard_deviation_intensity4->set_disp_level(Tango::EXPERT);
	att_list.push_back(standard_deviation_intensity4);

	//	Attribute : horizontalPosition
	horizontalPositionAttrib	*horizontal_position = new horizontalPositionAttrib();
	Tango::UserDefaultAttrProp	horizontal_position_prop;
	horizontal_position_prop.set_label("X");
	horizontal_position_prop.set_unit("mm");
	horizontal_position_prop.set_standard_unit("mm");
	horizontal_position_prop.set_display_unit("mm");
	horizontal_position_prop.set_format("%1.4e");
	horizontal_position_prop.set_description("horizontal position calculated as:\nX = Xfactor * ((I1 + I4) - (I2 + I3))/(sum) + Xoffset\nwhere:\n- I1 = (V1 + V1offset) * gain + I1offset\n- I2 = (V2 + V2offset) * gain + I2offset\n- I3 = (V3 + V3offset) * gain + I3offset\n- I4 = (V4 + V4offset) * gain + I4offset");
	horizontal_position->set_default_properties(horizontal_position_prop);
	att_list.push_back(horizontal_position);

	//	Attribute : verticalPosition
	verticalPositionAttrib	*vertical_position = new verticalPositionAttrib();
	Tango::UserDefaultAttrProp	vertical_position_prop;
	vertical_position_prop.set_label("Z");
	vertical_position_prop.set_unit("mm");
	vertical_position_prop.set_standard_unit("mm");
	vertical_position_prop.set_display_unit("mm");
	vertical_position_prop.set_format("%1.4e");
	vertical_position_prop.set_description("vertical position calculated as:\nZ = Zfactor * ((I1 + I2) - (I3 + I4))/(sum) + Zoffset\nwhere:\n- I1 = (V1 + V1offset) * gain + I1offset\n- I2 = (V2 + V2offset) * gain + I2offset\n- I3 = (V3 + V3offset) * gain + I3offset\n- I4 = (V4 + V4offset) * gain + I4offset");
	vertical_position->set_default_properties(vertical_position_prop);
	att_list.push_back(vertical_position);

	//	Attribute : gain
	gainAttrib	*gain = new gainAttrib();
	Tango::UserDefaultAttrProp	gain_prop;
	gain_prop.set_label("gain");
	gain_prop.set_unit(" ");
	gain_prop.set_standard_unit(" ");
	gain_prop.set_display_unit(" ");
	gain_prop.set_format("%1.4e");
	gain_prop.set_description(" the gain change according to the Locum4 input range:\n\nInput Range	gain\n(micro_A)            (A/V)\n1000	100	\n100	10\n10	1\n1	0.1\n0.1	0.01\n0.01	0.001		\n0.001	0.0001\n0.0001	0.00001");
	gain->set_default_properties(gain_prop);
	gain->set_disp_level(Tango::EXPERT);
	att_list.push_back(gain);

	//	Attribute : measurementUnit
	measurementUnitAttrib	*measurement_unit = new measurementUnitAttrib();
	Tango::UserDefaultAttrProp	measurement_unit_prop;
	measurement_unit_prop.set_label("current unity");
	measurement_unit_prop.set_unit(" ");
	measurement_unit_prop.set_standard_unit(" ");
	measurement_unit_prop.set_display_unit(" ");
	measurement_unit_prop.set_format(" ");
	measurement_unit_prop.set_description("displays the current measurement unit");
	measurement_unit->set_default_properties(measurement_unit_prop);
	att_list.push_back(measurement_unit);

	//	Attribute : enableAutoRange
	enableAutoRangeAttrib	*enable_auto_range = new enableAutoRangeAttrib();
	Tango::UserDefaultAttrProp	enable_auto_range_prop;
	enable_auto_range_prop.set_label("auto range");
	enable_auto_range_prop.set_unit(" ");
	enable_auto_range_prop.set_standard_unit(" ");
	enable_auto_range_prop.set_display_unit(" ");
	enable_auto_range_prop.set_description("true: enable auto range\nfalse: disable auto range");
	enable_auto_range->set_default_properties(enable_auto_range_prop);
	enable_auto_range->set_memorized();
	enable_auto_range->set_memorized_init(true);
	att_list.push_back(enable_auto_range);

	//	Attribute : quadrant1Spectrum
	quadrant1SpectrumAttrib	*quadrant1_spectrum = new quadrant1SpectrumAttrib();
	Tango::UserDefaultAttrProp	quadrant1_spectrum_prop;
	quadrant1_spectrum_prop.set_label("quadrant 1 buffer");
	quadrant1_spectrum_prop.set_unit(" ");
	quadrant1_spectrum_prop.set_standard_unit(" ");
	quadrant1_spectrum_prop.set_display_unit(" ");
	quadrant1_spectrum_prop.set_format(" ");
	quadrant1_spectrum_prop.set_description(" samples aqcuired on channel corresponding to quadrant1");
	quadrant1_spectrum->set_default_properties(quadrant1_spectrum_prop);
	att_list.push_back(quadrant1_spectrum);

	//	Attribute : quadrant2Spectrum
	quadrant2SpectrumAttrib	*quadrant2_spectrum = new quadrant2SpectrumAttrib();
	Tango::UserDefaultAttrProp	quadrant2_spectrum_prop;
	quadrant2_spectrum_prop.set_label("quadrant 2 buffer");
	quadrant2_spectrum_prop.set_description(" samples aqcuired on channel corresponding to quadrant2.");
	quadrant2_spectrum->set_default_properties(quadrant2_spectrum_prop);
	att_list.push_back(quadrant2_spectrum);

	//	Attribute : quadrant3Spectrum
	quadrant3SpectrumAttrib	*quadrant3_spectrum = new quadrant3SpectrumAttrib();
	Tango::UserDefaultAttrProp	quadrant3_spectrum_prop;
	quadrant3_spectrum_prop.set_label("quadrant 3 buffer");
	quadrant3_spectrum_prop.set_unit(" ");
	quadrant3_spectrum_prop.set_standard_unit(" ");
	quadrant3_spectrum_prop.set_display_unit(" ");
	quadrant3_spectrum_prop.set_format(" ");
	quadrant3_spectrum_prop.set_description(" samples aqcuired on channel corresponding to quadrant 3");
	quadrant3_spectrum->set_default_properties(quadrant3_spectrum_prop);
	att_list.push_back(quadrant3_spectrum);

	//	Attribute : quadrant4Spectrum
	quadrant4SpectrumAttrib	*quadrant4_spectrum = new quadrant4SpectrumAttrib();
	Tango::UserDefaultAttrProp	quadrant4_spectrum_prop;
	quadrant4_spectrum_prop.set_label("quadrant 4 buffer");
	quadrant4_spectrum_prop.set_unit(" ");
	quadrant4_spectrum_prop.set_standard_unit(" ");
	quadrant4_spectrum_prop.set_display_unit(" ");
	quadrant4_spectrum_prop.set_format(" ");
	quadrant4_spectrum_prop.set_description(" samples aqcuired on channel corresponding to quadrant4");
	quadrant4_spectrum->set_default_properties(quadrant4_spectrum_prop);
	att_list.push_back(quadrant4_spectrum);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void XbpmBeamLineClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	XbpmBeamLineClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void XbpmBeamLineClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "Locum4ProxyName";
	prop_desc = "Locum4 Proxy Name";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "Geometry";
	prop_desc = "you have to choose between 2 different modes, either insertion or bipolar mode.\n2 possible values\n- 1 : insertion\n- 2 : bipolar";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "LowVoltageThreshold";
	prop_desc = "if the input voltages of the ADC decrease this threshold, the corresponding\naverage current attributes become ALARM ( ex : if |Vmes| < low_threshold)";
	prop_def  = "0.9";
	vect_data.clear();
	vect_data.push_back("0.9");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "HighVoltageThreshold";
	prop_desc = "if the input voltages of the ADC exceed this threshold, the corresponding\naverage current attributes become ALARM ( ex : if |Vmes| > threshold)";
	prop_def  = "9.9";
	vect_data.clear();
	vect_data.push_back("9.9");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "CurrentOffset0";
	prop_desc = "current offset on channel 1";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "CurrentOffset1";
	prop_desc = "current offset on channel 2";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "CurrentOffset2";
	prop_desc = "current offset on channel 3";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "CurrentOffset3";
	prop_desc = "current offset on channel 4";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "VoltageOffset0";
	prop_desc = "voltage offset on channel 1";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "VoltageOffset1";
	prop_desc = "Voltage Offset on channel 2";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "VoltageOffset2";
	prop_desc = "Voltage Offset on channel3";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "VoltageOffset3";
	prop_desc = "Voltage Offset on channel 4";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "VerticalPositionFactor";
	prop_desc = "Vertical Position Factor\n";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "VerticalPositionOffset";
	prop_desc = "Vertical Position Offset";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "HorizontalPositionFactor";
	prop_desc = "Hortizontal Position Factor";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "HorizontalPositionOffset";
	prop_desc = "Hortizontal Position Offset";
	prop_def  = "0";
	vect_data.clear();
	vect_data.push_back("0");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "IntensityThreshold";
	prop_desc = "cuurent out of which the Measured Intensity has a meaning (in micro-A)";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "SaiControllerProxyName";
	prop_desc = "saiControllerProxyName";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "StartAtInit";
	prop_desc = "Launch <Start> command at Init. phase. [default = false]";
	prop_def  = "false";
	vect_data.clear();
	vect_data.push_back("false");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		XbpmBeamLineClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void XbpmBeamLineClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("XbpmBeamLine");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("This device calculates the vertical and horizontal positions of the beam:");
	str_desc.push_back(" <br>");
	str_desc.push_back("X = f (Ia,Ib,Ic Id,G,offsets,factors, geometry ...)");
	str_desc.push_back("Y = f (Ia,Ib,Ic Id,G,offsets,factors, geometry...)");
	str_desc.push_back(" <br>");
	str_desc.push_back("where:");
	str_desc.push_back("Ia =  current read on blade A <br>");
	str_desc.push_back("Ib =  current read on blade B <br>");
	str_desc.push_back("Ic =  current read on blade C <br>");
	str_desc.push_back("Id =  current read on blade D <br>");
	str_desc.push_back("G = locum4 amplifier gain <br>");
	str_desc.push_back("offsets & factors = attributes of the  device <br>");
	str_desc.push_back("geometry = property of the device <br>");
	str_desc.push_back(" <br>");
	str_desc.push_back("The calculation of beam position is the following: <br>");
	str_desc.push_back("for  square geometry <br>");
	str_desc.push_back("{	");
	str_desc.push_back("Qx = ((I0 + I3) - (I1 + I2))/Isum; <br>");
	str_desc.push_back("Qz = ((I0 + I1) - (I3 + I2))/Isum; <br>");
	str_desc.push_back("X = horizontalPositionFactor * Qx + horizontalPositionOffset; <br>");
	str_desc.push_back("Z = verticalPositionFactor * Qz + verticalPositionOffset; <br>");
	str_desc.push_back("}");
	str_desc.push_back("for cross geometry  <br>");
	str_desc.push_back("{");
	str_desc.push_back("Qx = (I1 - I0)/(I1 + I0); <br>");
	str_desc.push_back("Qz = (I2 - I3)/(I2 + I3); <br>");
	str_desc.push_back("X = horizontalPositionFactor * Qx  + horizontalPositionOffset; <br>");
	str_desc.push_back("Z = verticalPositionFactor * Qz +  verticalPositionOffset; <br>");
	str_desc.push_back("}");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs or svn location
	string	filename(classname);
	filename += "Class.cpp";
	
	// Create a string with the class ID to
	// get the string into the binary
	string	class_id(ClassId);
	
	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}
	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
